import React, { useState } from "react";

import Button from "../../components/Button"
import Games from '../../services/Games'
import ErrorManagerModal from '../../components/ErrorManagerModal'
const GAMES = new Games()

function AddGameInfo({ changeView, changeGameName }) {
  const [gameName, setGameName] = useState("")
  const [category, setCategory] = useState("")
  const [style, setStyle] = useState(["is-light", "is-light", "is-light", "is-light", "is-light", "is-light"])

  const [errorEmptyInfo, setErrorEmptyInfo] = useState(false)
  const [errorMessage, setErrorMessage] = useState(false)

  const handleChange = (event) => {
    setGameName(event.target.value)
  }

  const handleCategoryChange = (selectedCategory) => {

    setCategory(selectedCategory)
    let categories = ["Languages and frameworks", "Tools and libraries", "Methodologies", "Women coding", "Devscola", "Curiosities"]
    for (let item of categories) {
      let appearance = style
      let index = categories.indexOf(item)
      if (item != selectedCategory) {
        appearance[index] = "is-light"
      }
      if (item == selectedCategory) {
        appearance[index] = ""
      }
      setStyle(appearance)
    }
  }

  const saveGame = async () => {
    const gameInfo = {
      name: gameName,
      category: category,
    }
    const response = await GAMES.saveGame(gameInfo)
    if (response.status == 'fail') {
      setErrorMessage(response.data.message)
      setErrorEmptyInfo(true)
      return
    }
    changeView("BuildGame")
    changeGameName([gameName])
  }
  const handleClick = () => {
    if (gameName && category) {
      saveGame()
      return
    }
    setErrorMessage("You need a category and a game name ;(")
    setErrorEmptyInfo(true)
  }
  return (
    <div>
      <h1 className="title is-1" >Trivial Game</h1>
      <h3>Game Name:</h3>
      <input onChange={handleChange} className="input is-rounded" type="text" placeholder="Write the name here"></input>

      <h3>Choose the game's category:</h3>
      <Button buttonName="Languages and frameworks" handleClick={() => handleCategoryChange("Languages and frameworks")} className={`button is-primary ${style[0]}`} />
      <Button buttonName="Tools and libraries" handleClick={() => handleCategoryChange("Tools and libraries")} className={`button is-warning ${style[1]}`} />
      <Button buttonName="Methodologies" handleClick={() => handleCategoryChange("Methodologies")} className={`button is-link ${style[2]}`} />
      <Button buttonName="Women coding" handleClick={() => handleCategoryChange("Women coding")} className={`button is-danger ${style[3]}`} />
      <Button buttonName="Devscola" handleClick={() => handleCategoryChange("Devscola")} className={`button is-info ${style[4]}`} />
      <Button buttonName="Curiosities" handleClick={() => handleCategoryChange("Curiosities")} className={`button is-success ${style[5]}`} />


      <Button className='button' buttonName="Create" handleClick={handleClick} />
      <Button className='button' buttonName="Back" handleClick={() => changeView("Home")} />
      <ErrorManagerModal show={errorEmptyInfo} handleClose={() => setErrorEmptyInfo(false)} messageContent={errorMessage} />

    </div>

  )
}

export default AddGameInfo