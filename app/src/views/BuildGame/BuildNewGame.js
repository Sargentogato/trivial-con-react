import React from "react"

import Button from "../../components/Button"
import ErrorManagerModal from "../../components/ErrorManagerModal"
import Games from '../../services/Games'

const GAMES = new Games()

class BuildNewGame extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      question: "",
      answers: [""],
      correctAnswer: "",
      errorPopUp: false,
      errorMessageContent: ""
    }
  }

  handleSubmit = (event) => {
    event.preventDefault()
    if (this.state.answers.length >= 4){
      return this.setState({errorPopUp: true, errorMessageContent: 'The maximun number of answers is four'})
    }
    this.setState({ answers: this.state.answers.concat('') })
  }

  handleAnswerChange = (event, index) => {
    let answerList = [...this.state.answers]
    answerList[index] = event.target.value
    this.setState({ answers: answerList })
  }

  handleQuestionChange = (event) => {
    this.setState({ question: event.target.value })
  }

  handleCheckedChange = (event) => {
    this.setState({ correctAnswer: event.target.value })
  }

  futureFetch = () => { 
    if (!this.shouldShowErrorModal()){
      let ourQuestion = {
        gameName : this.props.gameName,
        gameQuestion: {
          question: this.state.question,
          answers: this.state.answers,
          correctAnswer: this.state.answers[this.state.correctAnswer]
        }
      }
      GAMES.saveQuestion(ourQuestion)
      
      this.setState({answers: [""], question: "", correctAnswer: ""})
    }
  }

  shouldShowErrorModal (){
    let message = ""
    
    
    if (this.hasCorrectAnswer ()) {
      message = 'You need to choose a correct answer'
    }
    if (this.hasMinimumAnswers()){
      message = 'You need at least two answers'
    }
   
    if (this.isQuestionEmpty()){
      message = 'You need to write a question'
    }

    if (this.hasDuplicatedAnswers ()) {
      message = "Don't write duplicated answers"
    }
    if (message !== ""){
      this.setState({errorMessageContent: message})
      this.setState({errorPopUp: true})
      return true
    }
    return false
  }

  isQuestionEmpty () {
    return this.state.question === ""
  }

  hasMinimumAnswers (){
    return this.state.answers.filter(element => element != "").length < 2
  }

  hasCorrectAnswer (){
    return this.state.correctAnswer === ""
  }

  hasDuplicatedAnswers (){
    let answersLowercase = this.state.answers.map(element => element.toLowerCase())
    return [...new Set(answersLowercase)].length != answersLowercase.length
  }

  render() {
    return (
      <div className="buildNewGame-container">
        <h1 className="title is-1" >Trivial Game</h1>
        <h3 className="title is-3" >Enter questions</h3>
        <form onSubmit={this.handleSubmit}>
          <label>
            Question:
            <br />
            <input className="input is-rounded" type="text" value={this.state.question} placeholder='enter' size='60' onChange={this.handleQuestionChange} />
            <br />
            <br />
            Answers:
            
            {this.state.answers.map((_, index) => {
              return (
                <div className="answer-container" key={index}>
                  <input name='choice' type="radio" value={index} defaultChecked={false} onChange={this.handleCheckedChange} />
                  <input className="input is-rounded" type="text" placeholder='answers' onChange={(event) => this.handleAnswerChange(event, index)} value={this.state.answers[index]} />
                </div>
              )
            })}
          </label>
          <input className="button is-info is-medium has-text-weight-bold mr-0 px-5" type="submit" value="Add" />
        </form>
        <br />
        <div className="buttons-buildNewGame">
          <Button className="button is-primary is-medium has-text-weight-bold" handleClick={() => this.futureFetch()} buttonName="Send question" />
          <Button className="button is-primary is-medium has-text-weight-bold" handleClick={() => this.props.changeView('Home')} buttonName='Done'/>
        </div>
        <ErrorManagerModal show={this.state.errorPopUp} handleClose ={() => 
          this.setState({errorPopUp: false})} messageContent = {this.state.errorMessageContent}/>
      </div>
    )
  }
}

export default BuildNewGame
