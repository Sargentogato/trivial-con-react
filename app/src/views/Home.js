import React from "react"

import Button from "../components/Button"
import GameManager from "./JoinGame/GameManager"
import SelectCategory from "./JoinGame/SelectCategory"
import BuildNewGame from "./BuildGame/BuildNewGame"
import Identifyer from "./Login/Identifyer"
import Register from "./Login/Register"
import Instructions from "./Instructions"
import EndGame from "./JoinGame/EndGame"
import AddGameInfo from "./BuildGame/AddGameInfo"

class Home extends React.Component {
  constructor(props) {
    super()
    this.state = {
      view: "Home",
      userName: "",
      gameName: "",
    }
  }

  changeView = (newView) => {
    this.setState({
      view: newView,
    })
  }

  nameChanged = (newName) => {
    this.setState({
      userName: newName,
    })
  }

  changeGameName = (newGameName) => {
    this.setState({
      gameName: newGameName,
    })
  }

  createView() {
    switch (this.state.view) {
      case "Home":
        return (
          <div className="container">
            <h1 className="title is-2">DEVSCOLA'S GAME</h1>
            <div>
              <h2 className="title is-2">Hello, {this.state.userName}</h2>
            </div>
            <div className="home-buttons">
              <Button className={"button is-primary is-medium has-text-weight-bold is-fullwidth"} handleClick={() => this.changeView("JoinGame")} buttonName="Join Game" />
              <Button className={"button is-primary is-medium has-text-weight-bold is-fullwidth"} handleClick={() => this.changeView("AddGameInfo")} buttonName="Build New Game" />
              <Button className={"button is-info is-medium has-text-weight-bold"} handleClick={() => this.changeView("Instructions")} buttonName="Instructions" />
            </div>
          </div>
        )
        break
      case "Identifyer":
        return <Identifyer nameChanged={this.nameChanged} changeView={this.changeView} />
        break
      case "Register":
        return <Register nameChanged={this.nameChanged} changeView={this.changeView} />
        break
      case "BuildGame":
        return <BuildNewGame changeView={this.changeView} gameName={this.state.gameName} />
        break
      case "JoinGame":
        return <SelectCategory changeView={this.changeView} userName={this.state.userName} changeGameName={this.changeGameName} />
        break
      case "GameManager":
        return <GameManager changeView={this.changeView} userName={this.state.userName} gameName={this.state.gameName} />
        break
      case "Instructions":
        return <Instructions changeView={this.changeView} />
        break
      case "EndGame":
        return <EndGame userName={this.state.userName} changeView={this.changeView} />
        break
      case "AddGameInfo":
        return <AddGameInfo changeView={this.changeView} changeGameName={this.changeGameName} />
        break
      default:
        break
    }
  }

  render() {
    let vista = this.createView()

    return <div>{vista}</div>
  }
}

export default Home
