import React from "react"
import Timer from '../../components/Timer'

function ScoreScreen({chosenAnswer, resultToAnswer, correctAnswer, userName, totalPoints, scoreScreenTimeOut}) {
  
  return (
    <div>
      <h1 className="title is-3">Trivial Game</h1>
      <div className="card pepita">
        <div className="card-content">
          <div className="content is-centered">
            <h3>Your answer was {chosenAnswer}</h3>
            <br />
            <h3>{resultToAnswer}</h3>
            {correctAnswer !== chosenAnswer && <h3>The right answer was: {correctAnswer}</h3> }
            <br />
            <h3>{userName} your points are:{totalPoints}</h3>
          </div>
          <footer className="card-footer">
            <span className="card-footer-item">
                <i className="fas fa-spinner fa-pulse"></i>
              <Timer runOutOfTime={scoreScreenTimeOut}/>
            </span>
          </footer>
        </div>
      </div>
    </div>
  )
}

export default ScoreScreen