import React from "react";
import Button from "../../components/Button"

function EndGame({ userName, changeView, totalPoints }) {

  return (
    <div>
        <h3 className="title is-3">Game done!</h3>
        {/* <p>{userName} your total points are:{totalPoints}</p> */}
        <Button handleClick={() => changeView('Home')} buttonName="Back to home" className="button is-primary"/>  
    </div>
  )
}

export default EndGame;
