import React from 'react'

import ScoreScreen from './ScoreScreen'
import GameScreen from './GameScreen'

class GameManager extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      correctAnswer: '',
      chosenAnswer: "",
      resultScreen: false,
      resultToAnswer: "",
      points: [],
      questionIndex: 0,
      userName: props.userName
    }
  }

  changeResultScreenValues = (resultSentence, points, correctAnswer, chosenAnswer) => {
    this.setState({ resultToAnswer: resultSentence })
    this.setState({ points: this.state.points.concat([points]) })
    this.setState({ correctAnswer: correctAnswer})
    this.setState({ chosenAnswer: chosenAnswer })
    this.switchResultScreen()
  }

  switchNextQuestion =()=>{
    const previousIndex = this.state.questionIndex 
    this.setState({questionIndex: previousIndex +1 })
  }

  switchResultScreen = () => {this.setState({ resultScreen: !this.state.resultScreen })}

  getTotalPoints = () => {
    return this.state.points.reduce((a,v)=>a+v)
  }

  scoreScreenTimeOut = () => {
    this.switchNextQuestion()
    this.switchResultScreen()
  }

  render(){
    if (this.state.resultScreen){
      return (
        <ScoreScreen 
          chosenAnswer={this.state.chosenAnswer} 
          resultToAnswer={this.state.resultToAnswer} 
          correctAnswer={this.state.correctAnswer}
          userName={this.state.userName}
          totalPoints={this.getTotalPoints()}
          scoreScreenTimeOut={this.scoreScreenTimeOut}
        />
      )
    }
    
    return (
      <GameScreen 
        questionIndex={this.state.questionIndex}
        changeResultScreenValues={this.changeResultScreenValues}
        userName={this.state.userName}
        changeView = {this.props.changeView}
        gameName= {this.props.gameName}
      />
    )
  }
    
}

export default GameManager
