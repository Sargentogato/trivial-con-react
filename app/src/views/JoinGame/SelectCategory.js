import React, { useState } from "react";
import Button from "../../components/Button"
import Games from '../../services/Games'

const game = new Games()

function SelectCategory({ userName, changeView, changeGameName }) {
  const [games, setGames] = useState([])

  const showCategoryName = async (category) => {
    const response = await game.selectCategory(category)
    if (response.status =="error") {
      return alert(response.message)
    }
    setGames(response.data[0].name)

  }

  return (
    <div className="select-category-container">
      <h1 className='title is-1'>DevTrivia</h1>
      <div className="columns is-centered select-category">
        <div className="categoryColumn column">
          <h3 className='is-centered'>{userName}, choose the game's category to play:</h3>
          <Button buttonName="Languages and frameworks" handleClick={() => showCategoryName("Languages and frameworks")} className="button is-primary is-outlined" />
          <Button buttonName="Tools and libraries" handleClick={() => showCategoryName("Tools and libraries")} className="button is-info is-outlined" />
          <Button buttonName="Methodologies" handleClick={() => showCategoryName("Methodologies")} className="button is-primary is-outlined" />
          <Button buttonName="Women coding" handleClick={() => showCategoryName("Women coding")} className="button is-primary is-outlined" />
          <Button buttonName="Devscola" handleClick={() => showCategoryName("Devscola")} className="button is-primary is-outlined" />
          <Button buttonName="Curiosities" handleClick={() => showCategoryName("Curiosities")} className="button is-primary is-outlined" />
        </div>
        <div className='gamesColumn card column'>
          {games.map((game, index) => {
            return (<span className='tag is-info' key={index} onClick={() => { changeView("GameManager"), changeGameName(game) }}>{game}</span>)
          })}
        </div>
      </div>
      <Button className='button' buttonName="Back" handleClick={() => changeView("Home")} />
    </div>
  )

}




export default SelectCategory