import React, { useState, useEffect } from "react";

const INTERVAL_IN_SECONDS = 1;
const INTERVAL_IN_MILISECONDS = INTERVAL_IN_SECONDS * 1000;
const END_OF_COUNTDOWN = 0;

function Timer({ runOutOfTime, className }) {

  const [timer, setTimer] = useState(10);


  useEffect(() => {

    countDown();

    return () => clearTimeout(advanceTime);

  }, [timer]);

  const reduceTime = () => setTimer(timer - INTERVAL_IN_SECONDS);

  const hasTimeLeft = () => timer > END_OF_COUNTDOWN;

  const advanceTime = setTimeout(() => {
    reduceTime();
  }, INTERVAL_IN_MILISECONDS);

  const countDown = () => hasTimeLeft() ? advanceTime : runOutOfTime();

  return <p className={className}>Time left: {timer}</p>;
}
export default Timer;