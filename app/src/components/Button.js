import React from "react";

function Button({handleClick, buttonName, className}) {
  return (
    <button className={className} onClick={() => handleClick()}>{buttonName}</button>
  )
}

export default Button;
