import "regenerator-runtime/runtime"
import { get, post } from './HTTPRequester'

class Users{
async registerNewUser(userAndPassword){
  const response = await post(`/registerNewUser`, userAndPassword)
  return response
}

async login(userAndPassword){
  const response = await post(`/login`, userAndPassword)
  return response
}
}

export default Users