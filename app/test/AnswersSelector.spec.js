import React from "react"
import { render } from "@testing-library/react"
import AnswersSelector from "../components/AnswersSelector.js"

describe("Answers", () => {
  it("shows players the answers", () => {
    const { getByText} = render(<AnswersSelector answersList={["Sí", "No"]} />)

    getByText(/Sí/i)
    getByText(/No/i)
  })
})