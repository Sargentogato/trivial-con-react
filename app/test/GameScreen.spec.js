import React from "react"
import fetchMock from "fetch-mock"
import { render, waitFor } from "@testing-library/react"
import GameScreen from "../components/GameScreen.js"

describe("Game Screen", () => {
  it("renders the question and its answers", async () => {

    const questions = [{
      question: "A, B, C?",
      answers: ["A", "B", "C"],
      correctAnswer: "A"
    }]

    fetchMock.mock(`${process.env.API_URL}/retrieveQuestions`, questions);
  
      const { getByText} =  render(<GameScreen questionIndex={0}/>)
      
      await waitFor( () => {getByText(/A, B, C/i)})

    fetchMock.restore();
  })
})