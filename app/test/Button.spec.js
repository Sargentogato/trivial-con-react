import React from "react"
import { render, fireEvent } from "@testing-library/react"
import Button from "../components/Button"

describe("Button", () => {
  it("it renders his text", () => {
    const { getByText} = render(<Button buttonName="Send" />)

    getByText(/send/i)
  });

  it("it handles a click", () => {
    const callback = jest.fn()
    const { getByText } = render(<Button handleClick={callback} buttonName="Send" />)

    const button = getByText(/send/i)
    fireEvent.click(button)

    expect(callback).toHaveBeenCalled()
  })
})


