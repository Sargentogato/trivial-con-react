import React from "react"
import { render, fireEvent } from "@testing-library/react"

import AddGameInfo from "../components/AddGameInfo"

describe("AddGameInfo", () => {
  it("it renders the component", () => {
    const { getByText} = render(<AddGameInfo  />)

    getByText(/Game Name/i)
    getByText(/Methodologies/i)
    getByText(/Create/i)
    getByText(/Back/i)
  })

  it("handles the input when it changes", () => {
    const { container } = render(<AddGameInfo  />)
    const input = container.querySelector('input')

    fireEvent.change(input, { target: { value: 'New Game' } })

    expect(input.value).toBe('New Game')
  })

  it("it goes back to home when back button is clicked", () => {
    const callback = jest.fn()
    const { getByText } = render(<AddGameInfo changeView={callback}/>)

    const button = getByText(/Back/i)
    fireEvent.click(button)

    expect(callback).toHaveBeenCalledWith("Home")
  })

  it("it goes to buildNewGame when create button is clicked", () => {
    const changeGameName = jest.fn()
    const changeView = jest.fn()
    const { container, getByText } = render(<AddGameInfo changeView={changeView} changeGameName={changeGameName}/>)
    const gameNameInput = container.querySelector('[placeholder="Write the name here"]')

    fireEvent.change(gameNameInput, { target: { value: 'Punset Game'} })
    const button = getByText(/Create/i)
    const categoryButton = getByText(/Methodologies/i)
    fireEvent.click(categoryButton)
    fireEvent.click(button)

    expect(changeView).toHaveBeenCalledWith("BuildGame")
    expect(changeGameName).toHaveBeenCalledWith(["Methodologies", 'Punset Game'])
  })
})