const cors = require('cors')
const express = require("express")
const app = express()
const port = process.env.PORT || 8081
const UsersService = require('./services/Users')
const usersService = new UsersService()
const GameService = require('./services/Games')
const gameService = new GameService()

app.use(express.json({ limit: '50mb' }))
app.use(cors())

let userPoints = {}

app.get('/', (req, res) => {
  res.send('Trivial Game')
})

app.get("/retrieveGames", async function (request,response){
  const category = request.query.category
  const result = await gameService.retrieveGames(category)
  
  response.json(result)
})

app.get("/retrieveQuestions", async function (request, response) {
  const gameName = request.query.gameName
  const result = await gameService.retrieveQuestions(gameName)
  
  response.json(result)
})

app.post("/saveGame", async function (request, response){
  let gameInfo = request.body
  const result = await gameService.saveGame(gameInfo)

  response.json(result)
})

app.post('/saveQuestion', async function (request, response) {
  const question = request.body.gameQuestion
  const name = request.body.gameName
  const result = await gameService.saveQuestion(question, name)

  response.json(result)
})

app.post('/userPoints', function (request, response) {
  let name = request.body.userName
  let points = request.body.points
  userPoints[name] = points
  response.json([])
})

app.post('/registerNewUser', async function (request, response) {
  const result = await usersService.registerNewUser(request.body)
  response.json(result)
})

app.post('/login', function (request, response) {
  const result = usersService.login(request.body)
  response.json(result)
})

app.listen(port, function () {
})

