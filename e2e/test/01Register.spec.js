import { Register } from '../pageObjects/register.js'

describe('Register', () => {

  let  register = new Register()

  beforeEach(() => {
    cy.visit('/')
    cy.wait(200)

  })
  
  it('allows the user to go back', () => {
    register.clickRegisterButton()
    register.clickBackButton()
    cy.get("#root").contains('Please log in')

  })

  it('allows the user to register', () => {
   register.clickRegisterButton()
   register.writeUserName()
   register.writePassword()
   register.writePasswordConfirmation()
   register.clickCreateNewUserButton()
   cy.get("#root").contains('Hello')
  })
})