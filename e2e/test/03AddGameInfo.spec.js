import { AddGameInfo } from '../pageObjects/addGameInfo'
import { Identifyer } from '../pageObjects/identifyer'

describe('Trivial:Add Game Info', () => {
  let addGameInfo = new AddGameInfo()
  let identifyer = new Identifyer()

  beforeEach(() => {
    cy.visit("localhost:1234")
    cy.wait(200)
  })

  it('allows users to specify category and game name', () => {
    identifyer.accessGame()
    cy.wait(600)
    addGameInfo.clickNewGame()
    cy.wait(600)
    addGameInfo.typeGameName('Trivial Mania')
    addGameInfo.selectCategory()
    addGameInfo.clickCreateGame()
    cy.get("#root").contains('Enter questions')
  })
})