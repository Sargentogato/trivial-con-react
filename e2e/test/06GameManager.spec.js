import { BuildNewGame } from '../pageObjects/buildNewGame.js'
import { GameManager} from '../pageObjects/gameManager.js'
import { Identifyer } from '../pageObjects/identifyer.js'
import { SelectCategory } from '../pageObjects/selectCategory.js'
import { AddGameInfo } from '../pageObjects/addGameInfo'

describe('Trivial: GameManager', () => {
    let selectCategory = new SelectCategory()
    let buildNewGame = new BuildNewGame()
    let gameManager = new GameManager()
    let identifyer = new Identifyer()
    let addGameInfo = new AddGameInfo()

    beforeEach(() => {
        cy.clock()
        cy.visit('/')
        cy.wait(200)

    })

    it('check if can send a right answer', () => {
        identifyer.accessGame()
        cy.wait(200)
        selectCategory.accessGame('Trivial Game III')
        cy.wait(200)
        gameManager.checkCorrectAnswer()
        gameManager.clickButton()

        cy.get('#root')
            .contains('You were right!')
    })

    it('check if can send a wrong answer', () => {
        identifyer.accessGame()
        cy.wait(200)

        selectCategory.accessGame('Trivial Game III')
        gameManager.checkWrongAnswer()
        gameManager.clickButton()

        cy.get('#root')
            .contains('You were wrong!')
    })

    it('send answer after time out', () => {
        identifyer.accessGame()
        cy.wait(200)

        selectCategory.accessGame('Trivial Game III')
        gameManager.checkWrongAnswer()
        
        cy.tick(10500)

        cy.get('#root')
            .contains('You were wrong!')
    })

    it('loads another question after answering one', () => {
        identifyer.accessGame()
        cy.wait(200)
        addGameInfo.createGame('Punset Game')
        buildNewGame.sendAQuestion()
        buildNewGame.sendAQuestion()
        buildNewGame.returnToMainMenu()
        selectCategory.accessGame('Punset Game')
        gameManager.checkWrongAnswer()
        gameManager.clickButton()

        cy.tick(10500)

        buildNewGame.checkExistingQuestion()
    })

    it('increases your points after answering right', () => {
        identifyer.accessGame()
        cy.wait(200)

        selectCategory.accessGame('Punset Game')
        gameManager.checkCorrectAnswer()
        gameManager.clickButton()

        cy.tick(10500)

        gameManager.checkCorrectAnswer()
        gameManager.clickButton()

        cy.get('#root')
            .contains('points are:2')
    })
})