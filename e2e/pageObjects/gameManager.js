const CORRECT_ANSWER = "NO"
const WRONG_ANSWER = "YES"

class GameManager {

  clickGameManager() {
    cy.get("button")
      .contains('Join Game')
      .click()
  }

  clickButton(){
    cy.get('button')
      .click()
  }

  checkWrongAnswer(){
    cy.get('[type="radio"]')
      .check(WRONG_ANSWER)
  }
  checkCorrectAnswer(){
    cy.get('[type="radio"]')
    .check(CORRECT_ANSWER)
  }

}

module.exports = { GameManager }