
const USER_NAME = 'El Sagan'
const PASSWORD = '123456'
const ID_INPUT = '[placeholder="Choose a cool name"]'
const PASSWORD_INPUT = '[placeholder="Enter your password"]'
const BUTTON_TEXT = 'Sign in'

class Identifyer {
  writeUserName(){
    cy.get(ID_INPUT).type(USER_NAME)
  }

  writePassword(){
    cy.get(PASSWORD_INPUT).type(PASSWORD)
  }

  clickSignInButton(){
    cy.get("button")
    .contains(BUTTON_TEXT)
    .click()
  }

  accessGame(){
    this.writeUserName()
    this.writePassword()
    this.clickSignInButton()
  }
  
}

module.exports = { Identifyer }